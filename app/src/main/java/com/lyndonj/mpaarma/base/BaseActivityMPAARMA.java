package com.lyndonj.mpaarma.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

@SuppressLint("Registered")
public class BaseActivityMPAARMA extends AppCompatActivity {

    CompositeDisposable cp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cp = new CompositeDisposable();
    }

    public void addRx(Disposable d){
        cp.add(d);
    }

    @Override
    protected void onDestroy() {
        cp.clear();
        super.onDestroy();
    }
}
