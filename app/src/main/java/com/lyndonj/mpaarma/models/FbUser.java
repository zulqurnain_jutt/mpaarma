package com.lyndonj.mpaarma.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class FbUser {
    public String uid;
    public boolean isBike; // true if bike , false if car or other

    public FbUser() {
    }

    public FbUser(String uid, boolean isBike) {
        this.uid = uid;
        this.isBike = isBike;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("isBike", isBike);
        return result;
    }
}
