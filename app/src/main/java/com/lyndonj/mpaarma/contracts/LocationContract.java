package com.lyndonj.mpaarma.contracts;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by sdsol on 27/09/2017.
 */

@EBean
public class LocationContract {

    public static final String TAG = "LocationContract";

    @RootContext
    Activity compat;

    private RxLocation rxLocation;
    private LocationRequest locationRequest;
    private LocationUpdates locationUpdates;
    private CompositeDisposable lcd;


    @AfterInject
    void init__(){
        rxLocation = new RxLocation(compat);

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setFastestInterval(2000)
                .setInterval(5000);

        lcd = new CompositeDisposable();
    }

    public void setOnLocationUpdates(LocationUpdates locationUpdates) {
        this.locationUpdates = locationUpdates;
    }

    public LocationRequest getRequest(int seconds){
        return LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(seconds * 1000);
    }


    public void startLocationRefresh() {
        lcd.clear();
        lcd.add(
                rxLocation.settings().checkAndHandleResolution(locationRequest)
                        .flatMapObservable(this::getAddressObservable)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(address -> {
                                    Log.d(TAG, "startLocationRefresh: address:"+address);
                                    locationUpdates.onAddressUpdate(address);
                                },
                                throwable -> Log.d("startLocationRefresh", "Error fetching location/address updates"+throwable))
        );
    }

    public void startLocationRefresh(LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
        lcd.clear();
        lcd.add(
                rxLocation.settings().checkAndHandleResolution(locationRequest)
                        .flatMapObservable(this::getAddressObservable)
//                        .flatMap(address -> {
//                            if(address == null){
//                                return getAddressObservableOldWay(true);
//                            }
//                            return Observable.error(new Throwable("No Address found"));
//                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(address -> {
                                    Log.d(TAG, "startLocationRefresh: address:"+address);
                                    locationUpdates.onAddressUpdate(address);
                                },
                                throwable -> Log.d("startLocationRefresh", "Error fetching location/address updates"+throwable))
        );
    }

    @SuppressWarnings({"MissingPermission"})
    private Observable<Address> getAddressObservable(boolean success) {
        if (success) {
            return rxLocation.location().updates(locationRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(location1 -> locationUpdates.onLocationUpdate(location1))
                    .flatMap(this::getAddressFromLocation);

        } else {
            locationUpdates.onLocationSettingsUnsuccessful();

            return rxLocation.location().lastLocation()
                    .doOnSuccess(location1 -> locationUpdates.onLocationUpdate(location1))
                    .flatMapObservable(this::getAddressFromLocation);
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private Observable<Address> getAddressObservableOldWay(boolean success) {
        if (success) {
            return rxLocation.location().updates(locationRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(location1 -> locationUpdates.onLocationUpdate(location1))
                    .observeOn(Schedulers.io())
                    .flatMap(this::geocodeOldWay);

        } else {
            locationUpdates.onLocationSettingsUnsuccessful();

            return rxLocation.location().lastLocation()
                    .doOnSuccess(location1 -> locationUpdates.onLocationUpdate(location1))
                    .observeOn(Schedulers.io())
                    .flatMapObservable(this::geocodeOldWay);
        }
    }

    private Observable<Address> getAddressFromLocation(Location location) {

        return rxLocation.geocoding().fromLocation(location).toObservable()
                .subscribeOn(Schedulers.io());
    }

    private Observable<Address> geocodeOldWay(Location location){
        // Setup Geocoder
        Geocoder geocoder = new Geocoder(compat.getApplicationContext(), Locale.getDefault());
        List<Address> addresses;

        // Attempt to Geocode from place lat & long
        try {

            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);

            if (addresses.size() > 0) {

                // Here are some results you can geocode
                String ZIP;
                String city;
                String state;
                String country;

                if (addresses.get(0).getPostalCode() != null) {
                    ZIP = addresses.get(0).getPostalCode();
                    Log.d("ZIP", ZIP);
                }

                if (addresses.get(0).getLocality() != null) {
                    city = addresses.get(0).getLocality();
                    Log.d("city", city);
                }

                if (addresses.get(0).getAdminArea() != null) {
                    state = addresses.get(0).getAdminArea();
                    Log.d("state", state);
                }

                if (addresses.get(0).getCountryName() != null) {
                    country = addresses.get(0).getCountryName();
                    Log.d("country", country);
                }

                return Observable.just(addresses.get(0));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Observable.error(new Throwable("No Address for given location.."));
    }

    public void addrx(Disposable d) {
        lcd.add(d);
    }

    public void clearx() {
        if (lcd != null) {
            lcd.dispose();
            lcd.clear();
        }
    }

    public interface LocationUpdates {
        void onLocationUpdate(Location location);

        void onAddressUpdate(Address address);

        void onLocationSettingsUnsuccessful();
    }
}