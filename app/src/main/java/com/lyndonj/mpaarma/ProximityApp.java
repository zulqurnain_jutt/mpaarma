package com.lyndonj.mpaarma;

import android.annotation.SuppressLint;
import android.support.multidex.MultiDexApplication;

import com.blankj.utilcode.util.Utils;
import com.google.firebase.FirebaseApp;

import org.androidannotations.annotations.EApplication;

@SuppressLint("Registered")
@EApplication
public class ProximityApp extends MultiDexApplication {
    @Override
    public void onCreate() {
        Utils.init(this);
        FirebaseApp.initializeApp(this);
        super.onCreate();
    }
}
