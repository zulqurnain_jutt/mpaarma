package com.lyndonj.mpaarma.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.lyndonj.mpaarma.R;
import com.lyndonj.mpaarma.base.BaseActivityMPAARMA;
import com.lyndonj.mpaarma.contracts.LocationContract;
import com.lyndonj.mpaarma.databinding.ActivityIdentifyBinding;
import com.lyndonj.mpaarma.models.FbUser;
import com.lyndonj.mpaarma.utils.LocalUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.BindingObject;
import org.androidannotations.annotations.DataBound;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.DrawableRes;
import org.androidannotations.annotations.res.StringRes;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("Registered")
@OptionsMenu(R.menu.proximity_menu)
@DataBound
@EActivity(R.layout.activity_identify)
public class IdentifyActivity extends BaseActivityMPAARMA implements
        OnMapReadyCallback,
        LocationContract.LocationUpdates {

    private int PROXIMITY_RADIUS_METERS = 10;

    @BindingObject
    ActivityIdentifyBinding binding;

    @Bean
    LocationContract locationContract;

    @FragmentById(R.id.map)
    SupportMapFragment supportMapFragment;

    @Extra
    boolean rememberLogin;

    @StringRes
    String proximity_speak_message;
    @StringRes
    String record_gpx;
    @StringRes
    String stop_gpx;
    @ColorRes
    int secondaryTextColor;
    @DrawableRes
    Drawable ic_bike;
    @DrawableRes
    Drawable ic_car;

    private GoogleMap mMap;
    private Location myLocation;
    private List<Location> locationsForGPX = new ArrayList<>();
    private FirebaseUser fbUser;
    private FbUser dbCurrentUser;
    private GeoFire geoFire;
    private Map<String, Marker> withinRangePins = new LinkedHashMap<>();
    private Map<String, FbUser> withinRangeUsers = new HashMap<>();
    private Map<String, Disposable> withinRangeDisposable = new HashMap<>();
    private DatabaseReference dbReference;
    private Marker myLocationMarker;
    private String fileGPXPath;
    private TextToSpeech tts_prox_alarm;
    private String dirLocalPath;
    private Circle myLocationRadiusCircle;
    private boolean isGPX_Recording = false;
    private GeoQuery geoQuery;
    private Disposable tts_disp;

    @AfterViews
    void init_av() {
        supportMapFragment.getMapAsync(this);
        // remove all previous activites
        ActivityUtils.finishOtherActivities(IdentifyActivity_.class);
        setSupportActionBar(binding.toolbar);

        //show error dialog if Google Play Services not available
        if (!LocalUtil.isGooglePlayServicesAvailable(this)) {
            finish();
        }

        init_vars(() -> {
            locationContract.startLocationRefresh();
        }); // initialize all variables

    }


    @OptionsItem(R.id.menu_logout)
    void logOutTheUser() {
        if (fbUser != null) {
            FirebaseAuth.getInstance().signOut();
            finish();
            LoginActivity_.intent(this).start();
        }
    }

    @OptionsItem(R.id.share_gfx)
    void shareLastGPXfile() {
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            File fileToShare = FileUtils.getFileByPath(fileGPXPath);

            if(fileToShare.exists()) {
                intentShareFile.setType("application/*");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+fileGPXPath));

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        "Sharing GPX File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing GPX File...");

                startActivity(Intent.createChooser(intentShareFile, "Share GPX File"));
            }else{
                SnackbarUtils.with(binding.clRoot)
                        .setMessage("No Last GPX file found yet")
                        .showWarning();
            }
        } catch (Exception e) {
            e.printStackTrace();
            SnackbarUtils.with(binding.clRoot)
                    .setMessage("Unable to share GPX , intent issue")
                    .showWarning();
        }
    }

    private void init_vars(Runnable everythingDone) {

        fbUser = FirebaseAuth.getInstance().getCurrentUser();
        if (fbUser == null) {
            finish();
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        dbReference = database.getReference().child("Users");
        geoFire = new GeoFire(database.getReference("geofire"));

        // create directory inits
        String phoneLocalPath = SDCardUtils.getSDCardPaths(false).get(0);
        dirLocalPath = phoneLocalPath + "/" + getString(R.string.app_name);
        fileGPXPath = dirLocalPath + "/" + "lastRoute.gpx";

        binding.btnRecord.setOnClickListener(v -> {
            if(isGPX_Recording){
                saveGPXFile(() -> {
                    binding.btnRecord.setText(record_gpx);
                    SnackbarUtils.with(binding.coordinatorRoot)
                            .setMessage("Successfully saved Location Logs in GPX file")
                            .showSuccess();
                });
            }else{
                binding.btnRecord.setText(stop_gpx);
                SnackbarUtils.with(binding.coordinatorRoot)
                        .setMessage("Started Recording Location Logs in GPX")
                        .showSuccess();
            }
            isGPX_Recording = !isGPX_Recording;
        });
        locationContract.setOnLocationUpdates(this);

        tts_prox_alarm = new TextToSpeech(this, status -> {
            if (status == TextToSpeech.SUCCESS) {
                int result = tts_prox_alarm.setLanguage(Locale.US);
                if (result == TextToSpeech.LANG_MISSING_DATA ||
                        result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    LogUtils.d("This Language is not supported");
                } else {
                    LogUtils.d("Text to speech module initialized successfully");
                }
            } else
                LogUtils.d("Initilization Failed!");
        });

        addRx(
                RxFirebaseDatabase.observeValueEvent(dbReference.child(fbUser.getUid()))
                        .subscribe(dataSnapshot -> {
                            if (dataSnapshot.exists()) {
                                dbCurrentUser = dataSnapshot.getValue(FbUser.class);
                                everythingDone.run();
                            } else {
                                SnackbarUtils.with(binding.clRoot)
                                        .setMessage("No Info found on DB for this User")
                                        .showError();
                            }
                        }, e -> {
                            e.printStackTrace();
                            LogUtils.e(e);
                            SnackbarUtils.with(binding.clRoot)
                                    .setMessage(e.getMessage())
                                    .showError();
                        })
        );
    }

    private void updateOrCreateRadiusCircle(LatLng myLocationLatLng) {
        if (myLocationRadiusCircle != null) {
            myLocationRadiusCircle.setCenter(myLocationLatLng);
        } else {
            myLocationRadiusCircle = mMap.addCircle(new CircleOptions()
                    .center(myLocationLatLng)
                    .radius(PROXIMITY_RADIUS_METERS)
                    .strokeColor(Color.parseColor("#334363a2"))
                    .fillColor(Color.parseColor("#33001447")));
        }
    }

    @Override
    public void onLocationUpdate(Location location) {
        LogUtils.d("lat:" + location.getLatitude() + " lng:" + location.getLongitude() + " accurate upto meters: " + location.getAccuracy());
        if(myLocation == null){
            hideProgress();
        }
        if (location.getAccuracy() >= 100 && myLocation != null) { // not accurate enough
            return;
        }
        myLocation = location;
        moveToMyLocation();
        getNearestUsers(PROXIMITY_RADIUS_METERS, myLocation);// position is important

        if(isGPX_Recording) {
            locationsForGPX.add(location);
        }else{
            locationsForGPX.clear();
        }
        try {
            geoFire.setLocation(
                    fbUser.getUid(),
                    new GeoLocation(myLocation.getLatitude(), myLocation.getLongitude())
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setTrafficEnabled(false);
        mMap.setIndoorEnabled(true);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        mMap.clear(); // clear all markers

        PermissionUtils.permission(PermissionConstants.LOCATION)
                .callback(new PermissionUtils.SimpleCallback() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onGranted() {
                        // Access to the location has been granted to the app.
                        mMap.setMyLocationEnabled(false);
                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                        showProgress();
                    }

                    @Override
                    public void onDenied() {
                        SnackbarUtils.with(binding.clRoot)
                                .setMessage("Location Permission Denied")
                                .showError();
                    }
                }).request();

        mMap.setOnMarkerClickListener(marker -> {
            if (marker.equals(myLocationMarker)) {
                marker.showInfoWindow();
            }
            return true;
        });

    }

    private io.reactivex.Observable<String> playVoiceAlarm(final TextToSpeech tts_prox_alarm, final String text) {
        return io.reactivex.Observable.create(emitter -> {
            emitter.setCancellable(() -> {
                if (tts_prox_alarm != null) {
                    tts_prox_alarm.stop();
                    tts_prox_alarm.setOnUtteranceProgressListener(null);
                }
            });
            if (!emitter.isDisposed()) {
                if (text == null || "".equals(text)) {
                    emitter.onError(new Throwable("string is empty"));
                } else {
                    if (tts_prox_alarm == null) {
                        emitter.onError(new Throwable("TTS object null"));
                        return;
                    }
                    tts_prox_alarm.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String s) {
                            emitter.onNext(s);
                        }

                        @Override
                        public void onDone(String s) {
                            emitter.onComplete();
                        }

                        @Override
                        public void onError(String s) {
                            emitter.onError(new Throwable(s));
                        }
                    });
                    tts_prox_alarm.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                }
            } else {
                if (tts_prox_alarm != null) {
                    tts_prox_alarm.stop();
                    tts_prox_alarm.setOnUtteranceProgressListener(null);
                }
            }
        });

    }

    public void moveToMyLocation() {
        if (myLocation == null) {
            return;
        }
        CameraUpdate factory = CameraUpdateFactory
                .newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                        19.0f);
        mMap.moveCamera(factory);
        //Move the camera to the user's location and zoom in!
        mMap.animateCamera(factory);
    }

    private void showProgress(){
        binding.rlPb.setVisibility(View.VISIBLE);
    }

    private void hideProgress(){
        binding.rlPb.setVisibility(View.GONE);
    }

    private void saveGPXFile(Runnable onSaved) {
        PermissionUtils.permission(PermissionConstants.STORAGE)
                .callback(new PermissionUtils.SimpleCallback() {
                    @Override
                    public void onGranted() {


                        if (FileUtils.createOrExistsDir(dirLocalPath)) {
                            if (FileUtils.createFileByDeleteOldFile(fileGPXPath)) {
                                LocalUtil.generateGpx(
                                        FileUtils.getFileByPath(fileGPXPath),
                                        "lastRoute",
                                        locationsForGPX,
                                        onSaved
                                );
                            } else {
                                LogUtils.d("Unable to lastRoute.gpx file");
                            }
                        } else {
                            LogUtils.d("Unable to directory " + dirLocalPath);
                        }
                    }

                    @Override
                    public void onDenied() {
                        SnackbarUtils.with(binding.clRoot)
                                .setMessage("Read/Write Permission Denied")
                                .showError();
                    }
                }).request();


    }


    private void getNearestUsers(double metersRadius, Location currLocation) {
        metersRadius = (metersRadius / 1000);
        if(geoQuery != null) {
            geoQuery.setCenter(new GeoLocation(currLocation.getLatitude(), currLocation.getLongitude()));
            return;
        }else {
            geoQuery = geoFire.queryAtLocation(new GeoLocation(currLocation.getLatitude(), currLocation.getLongitude()), metersRadius);
            geoQuery.removeAllListeners();
        }

        withinRangePins.clear();
        withinRangeUsers.clear();
        for (Map.Entry<String, Disposable> entry : withinRangeDisposable.entrySet()) {
            entry.getValue().dispose();
        }
        withinRangeDisposable.clear();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                if (key.equals(dbCurrentUser.uid)) {
                    processAproachingUser(key, dbCurrentUser, location);
                    return;
                }
                withinRangeDisposable.put(key,
                        RxFirebaseDatabase.observeValueEvent(dbReference.child(key))
                                .subscribe(dataSnapshot -> {
                                    if (dataSnapshot.exists()) {
                                        FbUser lastApproachingUser = dataSnapshot.getValue(FbUser.class);
                                        processAproachingUser(key, lastApproachingUser, location);
                                    }
                                }, e -> {
                                    e.printStackTrace();
                                    LogUtils.e(e);
                                })
                );

            }

            @Override
            public void onKeyExited(String key) {
                // user got out of radius
                if (withinRangeUsers.containsKey(key)) {
                    withinRangeUsers.remove(key); // remove from users
                }
                if (withinRangePins.containsKey(key)) {
                    withinRangePins.get(key).remove();
                    withinRangePins.remove(key); // remove marker
                    if(key.equals(dbCurrentUser.uid)){
                        myLocationMarker = null;
                    }
                }
                if (withinRangeDisposable.containsKey(key)) {
                    withinRangeDisposable.get(key).dispose();
                    withinRangeDisposable.remove(key);
                }
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                if (withinRangeUsers.containsKey(key)) { // user is already within radius
                    processAproachingUser(key, withinRangeUsers.get(key), location);
                }
            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                if (error != null) {
                    LogUtils.e(error);
                }
            }
        });
    }

    private void processAproachingUser(String approachUserKey, FbUser aprochedUser, GeoLocation approchingUserLoc) {

        if (!withinRangeUsers.containsKey(approachUserKey))
            withinRangeUsers.put(approachUserKey, aprochedUser);

        if (withinRangePins.containsKey(approachUserKey)) {
            Marker lastMarker = withinRangePins.get(approachUserKey);
            LocalUtil.animateMarkerTo(
                    lastMarker,
                    approchingUserLoc.latitude,
                    approchingUserLoc.longitude,
                    aprochedUser.isBike ? ic_bike : ic_car
            );
        } else {
            Marker newMarker = LocalUtil.addCustomPinToMap(
                    new LatLng(approchingUserLoc.latitude, approchingUserLoc.longitude),
                    mMap,
                    aprochedUser.isBike ? ic_bike : ic_car
            );
            withinRangePins.put(approachUserKey, newMarker);
        }

        Marker currMarker = withinRangePins.get(approachUserKey);

        if (approachUserKey.equals(dbCurrentUser.uid)) {
            myLocationMarker = currMarker;
            myLocationMarker.setTitle("Me");
            myLocationMarker.setSnippet("");
            myLocationMarker.showInfoWindow();
            updateOrCreateRadiusCircle(myLocationMarker.getPosition());
        }

        if (!dbCurrentUser.isBike && aprochedUser.isBike) {
            if(tts_disp == null) {
                tts_disp =
                        playVoiceAlarm(tts_prox_alarm, proximity_speak_message)
                                .flatMap(s -> playVoiceAlarm(tts_prox_alarm, s))
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .debounce(2, TimeUnit.SECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        s -> {
                                        },
                                        LogUtils::e,
                                        () -> LogUtils.d("TTS is not playing anymore")
                                );
            }
            SnackbarUtils.with(binding.coordinatorRoot)
                    .setMessage("Motorcycle is approaching...")
                    .setDuration(SnackbarUtils.LENGTH_INDEFINITE)
                    .setAction("Close",secondaryTextColor , v -> {
                        if (tts_prox_alarm != null) {
                            tts_prox_alarm.stop();
                        }
                        tts_disp.dispose();
                        tts_disp = null;
                    })
                    .showWarning();
        }
    }


    @Override
    public void onAddressUpdate(Address address) {
        LogUtils.d(address.toString());
    }

    @Override
    public void onLocationSettingsUnsuccessful() {
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        try {
            geoFire.removeLocation(fbUser.getUid());
            myLocationMarker.remove();
            tts_disp.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            tts_prox_alarm.shutdown();
        }
        if (!rememberLogin)
            FirebaseAuth.getInstance().signOut();
        locationContract.clearx();
        if(isGPX_Recording) {
            // save locations to gpx file
            saveGPXFile(() -> {
            });
        }
        super.onDestroy();
    }
}
