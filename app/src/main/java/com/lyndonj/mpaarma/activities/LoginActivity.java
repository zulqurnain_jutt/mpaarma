package com.lyndonj.mpaarma.activities;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.CompoundButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.blankj.utilcode.util.StringUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lyndonj.mpaarma.R;
import com.lyndonj.mpaarma.base.BaseActivityMPAARMA;
import com.lyndonj.mpaarma.databinding.ActivityLoginBinding;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.BindingObject;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.DataBound;
import org.androidannotations.annotations.EActivity;

import java.util.Objects;

import durdinapps.rxfirebase2.RxFirebaseAuth;
import durdinapps.rxfirebase2.RxFirebaseUser;

@SuppressLint("Registered")
@DataBound
@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivityMPAARMA {

    @BindingObject
    ActivityLoginBinding binding;
    private FirebaseAuth auth;

    @AfterViews
    void init_av() {
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            IdentifyActivity_.intent(this).rememberLogin(binding.cbRemember.isChecked()).start();
        }
    }

    @Click(R.id.btn_login)
    void loginUser() {
        showProgress();
        if (StringUtils.isEmpty(binding.edtEmail.getText()) || !RegexUtils.isEmail(binding.edtEmail.getText())) {
            binding.edtEmail.setError("Enter valid email...");
            hideProgress();
            return;
        }
        if (StringUtils.isEmpty(binding.edtPass.getText())) {
            binding.edtPass.setError("Enter valid password...");
            hideProgress();
            return;
        }

        addRx(
                RxFirebaseAuth.signInWithEmailAndPassword(
                        auth,
                        binding.edtEmail.getText().toString(),
                        binding.edtPass.getText().toString()
                ).subscribe(success -> {
                    IdentifyActivity_.intent(this).rememberLogin(binding.cbRemember.isChecked()).start();
                }, e -> {
                    hideProgress();
                    e.printStackTrace();
                    LogUtils.e(e);
                    SnackbarUtils.with(binding.clRoot)
                            .setMessage(e.getMessage())
                            .showError();
                }, () -> {
                })
        );
    }

    @Click(R.id.signup)
    void SignUpUser() {
        SignUpActivity_.intent(this).rememberLogin(binding.cbRemember.isChecked()).start();
    }

    @Click(R.id.forgot_pass)
    void sendRecoveryToUser() {

        MaterialDialog infoDialog =
                new MaterialDialog.Builder(this).build();

        MaterialDialog materialDialog =
                new MaterialDialog.Builder(this)
                        .input("Email...", "", false,
                                (dialog, input) -> FirebaseAuth.getInstance().sendPasswordResetEmail(input.toString())
                                        .addOnCompleteListener(task -> {
                                            infoDialog.setContent("Email is" + (task.isSuccessful() ? "" : "not") + " sent successfully...");
                                            dialog.dismiss();
                                            infoDialog.show();
                                        })).build();
        materialDialog.show();


    }

    private void showProgress(){
        binding.rlPb.setVisibility(View.VISIBLE);
    }

    private void hideProgress(){
        binding.rlPb.setVisibility(View.GONE);
    }
}
