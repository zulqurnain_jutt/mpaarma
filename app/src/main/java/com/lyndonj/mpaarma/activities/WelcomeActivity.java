package com.lyndonj.mpaarma.activities;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lyndonj.mpaarma.R;
import com.lyndonj.mpaarma.base.BaseActivityMPAARMA;
import com.lyndonj.mpaarma.databinding.ActivityWelcomeBinding;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.BindingObject;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.DataBound;
import org.androidannotations.annotations.EActivity;

@SuppressLint("Registered")
@DataBound
@EActivity(R.layout.activity_welcome)
public class WelcomeActivity extends BaseActivityMPAARMA {

    @BindingObject
    ActivityWelcomeBinding binding;

    @AfterViews
    void init_av(){

    }

    @Click(R.id.btn_proceed)
    void clickProceed(){
        LoginActivity_.intent(this).start();
    }

}
