package com.lyndonj.mpaarma.activities;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.view.View;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.SnackbarUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.lyndonj.mpaarma.R;
import com.lyndonj.mpaarma.base.BaseActivityMPAARMA;
import com.lyndonj.mpaarma.databinding.ActivitySignUpBinding;
import com.lyndonj.mpaarma.models.FbUser;
import com.lyndonj.mpaarma.utils.LocalUtil;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.BindingObject;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.DataBound;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import durdinapps.rxfirebase2.RxFirebaseAuth;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import durdinapps.rxfirebase2.RxFirebaseUser;
import io.reactivex.Observable;

@SuppressLint("Registered")
@DataBound
@EActivity(R.layout.activity_sign_up)
public class SignUpActivity extends BaseActivityMPAARMA {

    @BindingObject
    ActivitySignUpBinding binding;
    @Extra
    boolean rememberLogin;

    private FirebaseStorage fbstorage;
    private StorageReference storageReference;
    private Uri fileStoredUri;
    private FbUser userCreation;
    private FirebaseAuth fbAuth;
    private DatabaseReference fbDatabase;
    private FirebaseUser firebaseUser;

    @AfterViews
    void init_av() {
        Glide.with(this).load(R.drawable.ic_user_circle).into(binding.ivUserImg);
        fbstorage = FirebaseStorage.getInstance();
        fbAuth = FirebaseAuth.getInstance();
        fbDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        userCreation = new FbUser();

        binding.rgSep1.setOnCheckedChangeListener((radioGroup, i) -> {
            switch (i) {
                case R.id.radio_0:
                    if(binding.radio0.isChecked()){ // is car
                        userCreation.isBike = false;
                    }
                    break;
                case R.id.radio_1:
                    if(binding.radio1.isChecked()){ // is bike
                        userCreation.isBike = true;
                    }
                    break;
            }
        });

        binding.edtEmail.setText("r@r.com");
        binding.edtPass.setText("raja12");
        binding.edtCpass.setText("raja12");
        binding.edtFullname.setText("raja");

    }

    @Click(R.id.iv_user_img)
    void uploadImage() {

        LocalUtil.openBSImagePicker(
                this, (di, i) -> {
                    switch (i) {
                        case 1:
                            pickImageFromSource(Sources.GALLERY);
                            break;
                        case 0:
                            pickImageFromSource(Sources.CAMERA);
                            break;
                    }
                });
    }

    @Click(R.id.btn_signup)
    void signUpClick() {
        if (binding.pbImgUpload.isShown()) { // image is uploading
            SnackbarUtils.with(binding.clRoot)
                    .setMessage("Image is Uploading wait for it...")
                    .showWarning();
            return;
        }
        if (StringUtils.isEmpty(binding.edtEmail.getText()) || !RegexUtils.isEmail(binding.edtEmail.getText())) {
            binding.edtEmail.setError("Enter valid email...");
            return;
        }
        if (StringUtils.isEmpty(binding.edtFullname.getText())) {
            binding.edtFullname.setError("Enter valid name...");
            return;
        }
        if (StringUtils.isEmpty(binding.edtPass.getText())) {
            binding.edtPass.setError("Enter valid password...");
            return;
        }
        if (StringUtils.isEmpty(binding.edtCpass.getText()) ||
                binding.edtPass.getText().toString().compareTo(binding.edtCpass.getText().toString()) != 0) {
            binding.edtCpass.setError("Enter valid confirm password...");
            return;
        }
        binding.pbSignup.setVisibility(View.VISIBLE);

        // sign up firebase

        addRx(
                RxFirebaseAuth.createUserWithEmailAndPassword(
                        fbAuth,
                        binding.edtEmail.getText().toString(),
                        binding.edtPass.getText().toString())
                        .toObservable()
                        .flatMap(auth -> {
                            storageReference = fbstorage.getReference("users/images/" + TimeUtils.getNowMills() + ".jpg");
                            if (fileStoredUri != null) {
                                return RxFirebaseStorage.putFile(storageReference, fileStoredUri).toObservable();
                            } else {
                                return Observable.error(new Throwable("image is not available"));
                            }
                        })
                        .flatMap(tk -> RxFirebaseStorage.getDownloadUrl(storageReference).toObservable())
                        .map(uri -> {
                            if (uri == null) {
                                return new UserProfileChangeRequest.Builder()
                                        .setDisplayName(binding.edtFullname.getText().toString())
                                        .build();
                            }
                            return new UserProfileChangeRequest.Builder()
                                    .setDisplayName(binding.edtFullname.getText().toString())
                                    .setPhotoUri(uri)
                                    .build();
                        })
                        .subscribe(usercr -> {
                            firebaseUser = fbAuth.getCurrentUser();
                            if (firebaseUser == null) {
                                SnackbarUtils.with(binding.clRoot)
                                        .setMessage("Logged in User is null")
                                        .showError();
                            }
                            RxFirebaseUser.updateProfile(fbAuth.getCurrentUser(), usercr)
                                    .subscribe(() -> {
                                        userCreation.uid = firebaseUser.getUid();
                                        RxFirebaseDatabase.updateChildren(fbDatabase.child(firebaseUser.getUid()), userCreation.toMap())
                                                .subscribe(() -> {
                                                    binding.pbSignup.setVisibility(View.GONE);
                                                    IdentifyActivity_.intent(this).rememberLogin(rememberLogin).start();
                                                }, this::errorDisplay);
                                    }, this::errorDisplay);
                        }, this::errorDisplay)
        );
    }

    private void errorDisplay(Throwable e) {
        binding.pbSignup.setVisibility(View.GONE);
        if(fbAuth.getCurrentUser() !=null)fbAuth.getCurrentUser().delete();
        e.printStackTrace();
        LogUtils.e(e);
        SnackbarUtils.with(binding.clRoot)
                .setMessage(e.getMessage())
                .showError();
    }

    private void pickImageFromSource(Sources source) {
        binding.pbImgUpload.setVisibility(View.VISIBLE);
        addRx(
                RxImagePicker.with(getFragmentManager()).requestImage(source)
                        .subscribe(uri -> {
                            binding.pbImgUpload.setVisibility(View.GONE);
                            fileStoredUri = uri;
                            Glide.with(this).load(fileStoredUri).into(binding.ivUserImg);
                        }, e -> {
                            binding.pbImgUpload.setVisibility(View.GONE);
                            e.printStackTrace();
                            LogUtils.e(e);
                        })
        );
    }

}
