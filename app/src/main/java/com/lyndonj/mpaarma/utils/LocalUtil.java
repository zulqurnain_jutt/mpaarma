package com.lyndonj.mpaarma.utils;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.ColorInt;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lyndonj.mpaarma.R;

import org.androidannotations.annotations.res.DrawableRes;
import org.michaelbel.bottomsheet.BottomSheet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LocalUtil {

    public static void setUpWebViewThroughAssets(WebView wv, String drawableName) {
        // or "[path]/name.gif" (e.g: file:///android_asset/name.gif for resources in asset folder), and in loadDataWithBaseURL(), you don't need to set base URL, on the other hand, it's similar to loadData() method.
        String yourData = "<html style=\"margin: 0;\">\n" +
                "    <body style=\"margin: 0;\">\n" +
                "    <img src=" + drawableName + " style=\"width: 100%; height: 100%\" />\n" +
                "    </body>\n" +
                "    </html>";
// Important to add this attribute to webView to get resource from outside.
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setDomStorageEnabled(true);

// Notice: should use loadDataWithBaseURL. BaseUrl could be the base url such as the path to asset folder, or SDCard or any other path, where your images or the other media resides related to your html
        wv.loadDataWithBaseURL("file:///android_asset/", yourData, "text/html", "utf-8", null);
//        wv.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);

        wv.setBackgroundColor(ContextCompat.getColor(wv.getContext(), android.R.color.transparent));
        wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        wv.setVisibility(View.INVISIBLE);
    }

    public static void setUpWebViewThroughURL(WebView wv, String url) {
        // or "[path]/name.gif" (e.g: file:///android_asset/name.gif for resources in asset folder), and in loadDataWithBaseURL(), you don't need to set base URL, on the other hand, it's similar to loadData() method.
        String yourData = "<html style=\"margin: 0;\">\n" +
                "    <body style=\"margin: 0;\">\n" +
                "    <img src=" + url + " style=\"width: 100%; height: 100%\" />\n" +
                "    </body>\n" +
                "    </html>";
// Important to add this attribute to webView to get resource from outside.
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setDomStorageEnabled(true);

// Notice: should use loadDataWithBaseURL. BaseUrl could be the base url such as the path to asset folder, or SDCard or any other path, where your images or the other media resides related to your html
//        wv.loadDataWithBaseURL("file:///android_asset/", yourData, "text/html", "utf-8", null);
        wv.loadDataWithBaseURL(null, yourData, "text/html", "utf-8", null);

        wv.setBackgroundColor(ContextCompat.getColor(wv.getContext(), android.R.color.transparent));
        wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        wv.setVisibility(View.INVISIBLE);
    }

    public static void openBSImagePicker(Context context, DialogInterface.OnClickListener onclick) {
        BottomSheet.Builder builder = new BottomSheet.Builder(context);
        builder.setTitle(R.string.pick_image)
                .setItems(
                        new String[]{context.getString(R.string.camera), context.getString(R.string.gallery)},
                        new int[]{R.drawable.ic_camera, R.drawable.ic_gallery},
                        onclick)
                .setContentType(BottomSheet.LIST)
                .setDarkTheme(true)
                .setDividers(true)
                .show();
    }

    // Animation handler for old APIs without animation support
    public static void animateMarkerTo(final Marker marker, final double lat, final double lng,final Drawable drawablePin) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long DURATION_MS = 3000;
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final LatLng startPosition = marker.getPosition();
        handler.post(new Runnable() {
            @Override
            public void run() {
                float elapsed = SystemClock.uptimeMillis() - start;
                float t = elapsed/DURATION_MS;
                float v = interpolator.getInterpolation(t);

                double currentLat = (lat - startPosition.latitude) * v + startPosition.latitude;
                double currentLng = (lng - startPosition.longitude) * v + startPosition.longitude;
                udateCustomPinToMap(marker,new LatLng(currentLat, currentLng),drawablePin);

                // if animation is not finished yet, repeat
                if (t < 1) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }
    public static Marker addCustomPinToMap(LatLng latLng, GoogleMap gmap, Drawable drawablePin) {
        return gmap.addMarker(
                new MarkerOptions()
                        .position(latLng)
                        .icon(vectorToBitmap(drawablePin))
                        .visible(true)
//                        .rotation(-90)
                        .anchor(0.5f, 0.5f)
        );
    }

    public static void udateCustomPinToMap(Marker markerToUpdate, LatLng latLng, Drawable drawablePin) {
        markerToUpdate.setIcon(vectorToBitmap(drawablePin));
        markerToUpdate.setPosition(latLng);
        markerToUpdate.setVisible(true);
//        markerToUpdate.setRotation(getBearing(markerToUpdate.getPosition(),latLng));
        markerToUpdate.setAnchor(0.5f,0.5f);
    }

    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }


    public static BitmapDescriptor vectorToBitmap(Drawable vectorDrawable) {
        assert vectorDrawable != null;
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//        DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    // Checking if Google Play Services Available or not
    public static boolean isGooglePlayServicesAvailable(Activity myActivity) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(myActivity);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(myActivity, result, 0).show();
            }
            return false;
        }
        return true;
    }

    public static void generateGpx(File file, String name, List<Location> points, Runnable onDone) {

        String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><gpx xmlns=\"http://www.topografix.com/GPX/1/1\" creator=\"MapSource 6.15.5\" version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\"><trk>\n";
        name = "<name>" + name + "</name><trkseg>\n";

        StringBuilder segments = new StringBuilder();
        @SuppressLint("SimpleDateFormat")
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        for (Location location : points) {
            segments.append("<trkpt lat=\"").append(location.getLatitude()).append("\" lon=\"").append(location.getLongitude()).append("\"><time>").append(df.format(new Date(location.getTime()))).append("</time></trkpt>\n");
        }

        String footer = "</trkseg></trk></gpx>";

        try {
            FileWriter writer = new FileWriter(file, false);
            writer.append(header);
            writer.append(name);
            writer.append(segments.toString());
            writer.append(footer);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            Log.e("generateGpx", "Error Writting Path", e);
        }
        onDone.run();
    }
}
